package com.d.danishspicsumbrowser.modulepicsum;

import android.content.Context;
import android.os.PersistableBundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.d.danishspicsumbrowser.R;
import com.d.danishspicsumbrowser.modulepicsum.adapter.PicsumLibraryAdapter;
import com.d.danishspicsumbrowser.modulepicsum.async.GetImagesAsynch;
import com.d.danishspicsumbrowser.modulepicsum.async.ImagesFetch;
import com.d.danishspicsumbrowser.modulepicsum.model.PicsumModel;
import com.d.danishspicsumbrowser.utils.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PicsumActivity extends AppCompatActivity {

    private Context context;
    private ArrayList<PicsumModel> imageList;
    private PicsumLibraryAdapter adapter;
    private Snackbar snackbar;
    final String ORI_CONST = "orientation";

    @BindView(R.id.progressBar)
    ProgressBar progress_bar;

    @BindView(R.id.gv_library)
    GridView gv_library;

    @BindView(R.id.cl_root)
    ConstraintLayout cl_root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picsum);

        ButterKnife.bind(this);
        context = PicsumActivity.this;

        //check for orientation change
        if (savedInstanceState != null && savedInstanceState.containsKey(ORI_CONST) && savedInstanceState.getString(ORI_CONST) != null) {
            if (Objects.requireNonNull(savedInstanceState.getString(ORI_CONST)).equals("changed")) {
                imageList = savedInstanceState.getParcelableArrayList("list");
                adapter = new PicsumLibraryAdapter(context, imageList);
                gv_library.setAdapter(adapter);
            }
        } else {
            getImagesAPICall();
        }
    }

    //get images API call
    private void getImagesAPICall() {
        if (CommonUtils.isConnectionAvailable(context)) {
            CommonUtils.showProgressBar(true, progress_bar, getWindow());
            new GetImagesAsynch().getImagesAPI(context, CommonUtils.getImageListURL, new ImagesFetch() {
                @Override
                public void onImagesFetch(boolean isSuccessful, String response) {
                    CommonUtils.showProgressBar(false, progress_bar, getWindow());
                    if (isSuccessful) {
                        imageList = new ArrayList<>();
                        imageList = new Gson().fromJson(response, new TypeToken<ArrayList<PicsumModel>>() {
                        }.getType());
                        adapter = new PicsumLibraryAdapter(context, imageList);
                        gv_library.setAdapter(adapter);
                    } else {
                        Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            showNoInternetSnackBarError();
        }
    }

    //show error message if there is not internet connection
    private void showNoInternetSnackBarError() {
        snackbar = Snackbar
                .make(cl_root, "No Internet Connection!", Snackbar.LENGTH_INDEFINITE)
                .setAction("Reload", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (CommonUtils.isConnectionAvailable(context)) {
                            getImagesAPICall();
                            snackbar.dismiss();
                        } else {
                            showNoInternetSnackBarError();
                        }
                    }
                });

        snackbar.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(ORI_CONST, "changed");
        outState.putParcelableArrayList("list", imageList);
        super.onSaveInstanceState(outState);
    }
}