package com.d.danishspicsumbrowser.modulepicsum.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class PicsumModel implements Parcelable {

    private String format;
    private Integer width;
    private Integer height;
    private String filename;
    private Integer id;
    private String author;
    private String authorUrl;
    private String postUrl;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    public final static Parcelable.Creator<PicsumModel> CREATOR = new Creator<PicsumModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PicsumModel createFromParcel(Parcel in) {
            return new PicsumModel(in);
        }

        public PicsumModel[] newArray(int size) {
            return (new PicsumModel[size]);
        }

    };

    protected PicsumModel(Parcel in) {
        this.format = ((String) in.readValue((String.class.getClassLoader())));
        this.width = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.height = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.filename = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.author = ((String) in.readValue((String.class.getClassLoader())));
        this.authorUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.postUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.additionalProperties = ((Map<String, Object>) in.readValue((Map.class.getClassLoader())));
    }

    public PicsumModel() {
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorUrl() {
        return authorUrl;
    }

    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(format);
        dest.writeValue(width);
        dest.writeValue(height);
        dest.writeValue(filename);
        dest.writeValue(id);
        dest.writeValue(author);
        dest.writeValue(authorUrl);
        dest.writeValue(postUrl);
        dest.writeValue(additionalProperties);
    }

}