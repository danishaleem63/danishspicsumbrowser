package com.d.danishspicsumbrowser.modulepicsum.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.d.danishspicsumbrowser.R;
import com.d.danishspicsumbrowser.modulepicsum.model.PicsumModel;
import com.d.danishspicsumbrowser.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PicsumLibraryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PicsumModel> imageList = new ArrayList<>();

    public PicsumLibraryAdapter(Context context, ArrayList<PicsumModel> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_picsum, null);

        ImageView iv_icon = convertView.findViewById(R.id.iv_icon);
        TextView tv_imageName = convertView.findViewById(R.id.tv_imageName);

        tv_imageName.setText(imageList.get(position).getAuthor());

        Picasso.get().load(CommonUtils.getImageById + imageList.get(position).getId()).placeholder(R.drawable.loading).into(iv_icon);

        return convertView;
    }
}
