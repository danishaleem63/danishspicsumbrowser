package com.d.danishspicsumbrowser.modulepicsum.async;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

public class GetImagesAsynch {

    public void getImagesAPI(Context context, String URL, final ImagesFetch imagesFetchListener) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null && response.length() > 0) {
                    imagesFetchListener.onImagesFetch(true, response.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error.getMessage() != null) {
                    imagesFetchListener.onImagesFetch(false, error.getMessage());
                } else {
                    imagesFetchListener.onImagesFetch(false, "Something went wrong!!");
                }
            }
        });

        Volley.newRequestQueue(context).add(jsonArrayRequest);
    }
}
