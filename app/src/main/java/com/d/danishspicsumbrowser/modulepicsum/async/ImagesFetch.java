package com.d.danishspicsumbrowser.modulepicsum.async;

public interface ImagesFetch {
    void onImagesFetch(boolean isSuccessful, String response);
}
